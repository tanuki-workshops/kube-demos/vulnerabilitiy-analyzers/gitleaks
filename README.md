# gitleaks

See:
- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml
- https://docs.gitlab.com/ee/user/application_security/secret_detection/

## Use it

```yaml
stages:
  - 🔎secure

include:
  - project: 'tanuki-workshops/kube-demos/vulnerabilitiy-analyzers/gitleaks'
    file: 'gitleaks.gitlab-ci.yml'

👮‍♀️:secrets:detection:
  stage: 🔎secure
  extends: .gitleaks:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  script: |
    analyze ./
```
